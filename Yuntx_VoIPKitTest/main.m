//
//  main.m
//  Yuntx_VoIPKitTest
//
//  Created by 高源 on 2018/7/30.
//  Copyright © 2018年 高源. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
