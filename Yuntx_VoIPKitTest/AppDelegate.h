//
//  AppDelegate.h
//  Yuntx_VoIPKitTest
//
//  Created by 高源 on 2018/7/30.
//  Copyright © 2018年 高源. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

