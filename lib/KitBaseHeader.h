//
//  KitBaseHeader.h
//  VoipKitDemo
//
//  Created by 王文龙 on 2016/11/19.
//  Copyright © 2016年 王文龙. All rights reserved.
//

#ifndef KitBaseHeader_h
#define KitBaseHeader_h


#endif /* KitBaseHeader_h */
#import "ECError.h"

/**
 *  选人的场景
 */
typedef NS_ENUM(NSInteger, SelectObjectType){
    
    SelectObjectType_None = 0,//普通群聊
    
    SelectObjectType_VoiceMeetingSelectMember = 1011,//语音会议选人
    /*(调用ChatSelectMemberViewController【语音会议选人】)*/
    
    SelectObjectType_CreateVoiceMeetingSelectMember = 1012,//创建语音会议选人
    /*(调用ChatSelectMemberViewController【语音会议选人】)*/
    
    SelectObjectType_VideoMeetingSelectMember = 1021,//视频会议选人
    /*(调用ChatSelectMemberViewController【视频会议选人】)*/
    
    SelectObjectType_CreateVideoMeetingSelectMember = 1022,//创建视频会议选人
    /*(调用ChatSelectMemberViewController【视频会议选人】)*/
};
/**
 *  通话结束时用于判断通话类型
 */
typedef NS_ENUM(NSInteger, VoipCallType){
    
    VoipCallType_Voice = 0,//语音单聊
    
    VoipCallType_Video = 1,//视频单聊
    
    VoipCallType_VoiceMeeting = 20,//语音会议
    
    VoipCallType_VideoMeeting = 21,//视频会议
};
@protocol ComponentDelegate <NSObject>

@required

/**
 @brief 获取登录用户信息回调
 @return 登陆的用户信息字典
 */
-(NSDictionary*)onGetUserInfo;

/**
 @brief 获取用户信息回调
 @param Id 用户的account或者手机号
 @param type 查询类型  0,根据account获取，1根据手机号获取
 @param userData 透传字段
 @return 用户信息字典
 */
-(NSDictionary*)getDicWithId:(NSString*)Id withType:(int) type UserData:(NSString *)userData;

@optional

/**
 @brief 通话结束回调
 @param error 失败错误码穿回
 @param type 通话类型
 @param information 通话信息
 */
-(void)finishedCallWithError:(NSError *)error WithType:(VoipCallType)type WithCallInformation:(NSDictionary*)information   UserData:(NSString *)userData;

@end


/**
 * 重连状态值
 */
typedef NS_ENUM(NSUInteger,ECConnectState) {
    
    /** 连接成功 */
    State_ConnectSuccess=0,
    
    /** 连接中 */
    State_Connecting,
    
    /** 连接失败 */
    State_ConnectFailed
};
@protocol AppModelDelegate <NSObject>
@optional

/**
 @brief 登录状态回调
 @param state 登陆状态
 @param error 错误信息
 */
-(void)onConnectState:(ECConnectState)state  failed:(ECError*)error;
@end
